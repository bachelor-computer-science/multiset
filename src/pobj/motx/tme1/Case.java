package pobj.motx.tme1;

/**
 * Classe représentant une case avec un caractère et ses coordonées
 * @author abdoulaye
 *
 */
public class Case {
	/** Numéro de ligne de la case*/
	private int lig;
	/** Numéro de colonne de la case*/
	private int col;
	/** Caractère représentant la valeur de la case*/
	private char c;
	
	/**
	 * Construit une case initiale avec les parmetres
	 * @param lig
	 * @param col
	 * @param c
	 */
	public Case(int lig, int col, char c){
		this.lig= lig;
		this.col = col;
		this.c = c;
	}
	
	/**
	 * Accesseur du numéro de ligne
	 * @return
	 */
	public int getLig(){
		return lig;
	}
	
	/**
	 * Accesseur du numero de colonne
	 * @return
	 */
	public int getCol(){
		return col;
	}
	/**
	 * Accesseur du caractere present dans la case
	 * @return
	 */
	public char getChar(){
		return c;
	}
	/**
	 * modificateur du caractere contenu dans la case
	 * @param c
	 */
	public void setChar(char c){
		this.c = c;
	}
	/**
	 * verifie si la case est vide càd si elle a comme valeur espace " " 
	 * @return
	 */
	public boolean isVide(){
		return c == ' ';
	}
	/**
	 * verifie si la case est pleine càd si elle a comme valeur espace "*" 
	 * @return
	 */
	public boolean isPleine(){
		return c == '*';
	}
}
