package pobj.motx.tme1;
/**
 * Classe représentant une grille de cases
 * @author abdoulaye
 *
 */
public class Grille {
	/**
	 * Matrice de cases
	 */
	private Case [][] cases ;
	/**
	 * construire la grille hauteur X largeur
	 * @param hauteur
	 * @param largeur
	 */
	public Grille(int hauteur, int largeur){
		cases = new Case[hauteur][largeur];
		for(int i=0; i<cases.length; i++){
			for(int j=0; j<cases[i].length; j++){
				cases[i][j] = new Case(i, j, ' ');
			}
		}
	}
	/**
	 * Accesseur de la case se trouvant à la position (lig,col)
	 * @param lig
	 * @param col
	 * @return
	 */
	public Case getCase(int lig, int col){
		return cases[lig][col];
	}
	/**
	 * renvoie le nombre de ligne de la grille
	 * @return
	 */
	public int nbLig(){
		return cases.length;
	}
	/**
	 * renvoie le nombre de colonne de la grille
	 * @return
	 */
	public int nbCol(){
		return cases[0].length;
	}	
	/**
	 * méthode qui renvoie une copie de la grille courante
	 * @return
	 */
	public Grille copy(){
		Grille g = new Grille(this.nbLig(), this.nbCol());
		for(int i=0; i<this.nbLig(); i++){
			for(int j=0; j<this.nbCol(); j++){	
				g.getCase(i,j).setChar(this.getCase(i,j).getChar());
			}
		}
		return g;
	}
	/**
	 * décrire la grille
	 */
	@Override
	public String toString(){
		return GrilleLoader.serialize(this, false);
	}
	
}
