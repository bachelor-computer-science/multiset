package pobj.motx.tme1;

import java.util.*;
/**
 * classe représentant une grille de mots
 * @author abdoulaye
 *
 */
public class GrilleMots {
	/**
	 * liste de mots se trouvant dans la grille
	 */
	private List<Mot> mots;
	/**
	 * nombre de mots horizontaux dans la grille
	 */
	private int nbH; //nb de mots horizontaux
	/**
	 * grille de caractere
	 */
	private Grille g;
	
	/**
	 * construire une grille de mots à partir de la grille donnée
	 * @param gr
	 */
	public GrilleMots(Grille gr){
		mots = new ArrayList<Mot>();
		this.g = gr;
		/*chercher les mots horizontaux*/
		for(int i=0; i<g.nbLig(); i++){
			List<Case> c = getLig(i);
			chercherMots(c);
		}
		
		/*noter le nombre de mots horinzontaux*/
		nbH = mots.size();
		/*chercher les mots verticaux*/
		for(int i=0; i<g.nbCol(); i++){
			List<Case> c = getCol(i);
			chercherMots(c);
		}
	}
	
	public Grille getGrille(){
		return g;
	}
	
	/**
	 * accesseur du nombre de mots horizontaux
	 * @return
	 */
	public int getNbHorizontal(){
		return nbH;
	}
	/**
	 * accesseur de la liste de mots dans la grille
	 * @return
	 */
	public List<Mot> getMots(){
		return mots;
	}
	/**
	 * renvoie la liste de cases constituant la ligne lig
	 * @param lig
	 * @return
	 */
	private List<Case> getLig(int lig){
		List<Case> casesLig = new ArrayList<Case>();
		for(int j=0; j<g.nbCol(); j++){
			casesLig.add(g.getCase(lig, j));
		}
		return casesLig;		
	}
	/**
	 * renvoie la liste de cases constituant la colonne col
	 * @param col
	 * @return
	 */
	private List<Case> getCol(int col){
		List<Case> casesCol = new ArrayList<Case>();
		for(int i=0; i<g.nbLig(); i++){
			casesCol.add(g.getCase(i, col));
		}
		return casesCol;		
	}
	/**
	 * chercher les mots se trouvant sur une liste de case donnée
	 * @param cases
	 */
	private void chercherMots(List<Case> cases){
		
		Mot m = new Mot();
		for(int i=0; i<cases.size(); i++){
			Case c = cases.get(i);
			/*si la case est non pleine, ajoute dans m*/
			if(!(c.isPleine())){
				m.addCase(c);
			}
			/*sinon, un mot est composé d'au moins 2 lettres*/
			else{						
				if(m.size()>=2){
					/*ajouter le mot trouvé dans la listes de mots detecté*/
					mots.add(m);
					m = new Mot();
				}
				else{
					m = new Mot();
				}
			}
		}	
		/*traiter le dernier mot*/
		if(m.size()>=2)
			mots.add(m);
	}
	
	/**
	 * 
	 * @param m
	 * @param soluce
	 * @return
	 */
	public GrilleMots fixer(int m, String soluce){
		GrilleMots gr = new GrilleMots(g.copy());
		List<Mot> lm = gr.getMots();
		Mot mot = lm.get(m);
		for(int i=0; i<mot.size(); i++){
			//char c = soluce.charAt(i);
			mot.getLettres().get(i).setChar(soluce.charAt(i));
		}
		return gr;	
	}
	
	@Override
	public String toString(){
		String s = "";
		for(Mot m : mots){
			s += m.toString()+"\n";
		}
		return s;
	}
}
