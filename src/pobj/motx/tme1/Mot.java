package pobj.motx.tme1;

import java.util.*;
/**
 * Classe représentant un mot qui est une liste de lettres
 * @author abdoulaye
 *
 */
public class Mot {
	/**
	 * liste de cases contenant des caractères
	 */
	private List<Case> lettres;
	/**
	 * construire un mot vide
	 */
	public Mot(){
		lettres = new ArrayList<Case>();
	}
	/**
	 * décrire le mote
	 */
	@Override
	public String toString(){
		String s = "";
		for(Case c: lettres){
			s+=c.getChar();
		}
		return s;
	}
	/**
	 * accesseur de la liste de cases constituant le mot
	 * @return
	 */
	public List<Case> getLettres(){
		return lettres;
	}
	/**
	 * ajouter un caractere dans le mot
	 * @param c
	 */
	public void addCase(Case c){
		lettres.add(c);
	}
	/**
	 * renvoie la taille du mot
	 * @return
	 */
	public int size(){
		return lettres.size();
	}
}
