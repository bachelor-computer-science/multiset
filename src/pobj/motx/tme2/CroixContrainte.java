package pobj.motx.tme2;

public class CroixContrainte implements IContrainte{
	/**
	 * l'indice du mot1
	 */
	private int m1;
	/**
	 * l'indice du mot2
	 */
	private int m2;
	/**
	 * l'indice d'un caratere du mot1
	 */
	private int c1;
	/**
	 * l'indice d'un caractere du mot2
	 */
	private int c2;
	/**
	 * constructeur d'un croixContrainte
	 * @param m1 
	 * @param m2
	 * @param c1
	 * @param c2
	 */
	public CroixContrainte(int m, int n, int o, int p){
		m1 = m; m2 = n;  c1=o;   c2 = p;
	}
	/**
	 * filtre dictionnaires potentiels des mots se trouvant 
	 * respectvement à m1 et m2 de la liste en fonction de leur intersection 
	 * @param grille potentiel 
	 * @return nombre de motes filtrés
	 */
	public int reduce(GrillePotentiel grille) {
		int nbSupp = 0;
		Dictionnaire d1 = grille.getMotsPot().get(m1);
		Dictionnaire d2 = grille.getMotsPot().get(m2);
		EnsembleLettre ens1 = d1.filtreParIndice(c1);
		EnsembleLettre ens2 = d2.filtreParIndice(c2);
		EnsembleLettre inter = ens1.intersection(ens2);
		if(ens1.size() > inter.size())
			nbSupp += d1.filtreParEnsemble(inter, c1);
		if(ens2.size() > inter.size())
			nbSupp += d2.filtreParEnsemble(inter, c2);				
		
		return nbSupp;
	}	
}
