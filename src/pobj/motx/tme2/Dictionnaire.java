package pobj.motx.tme2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Un ensemble de mots.
 *
 */
public class Dictionnaire {

	// stockage des mots
	private List<String> mots = new ArrayList<>();
	/*mettre en cache certain caractere pour ne pas rechercher dans le dictionnaire*/
	private EnsembleLettre [] cache = null;
	/**
	 * Ajoute un mot au Dictionnaire, en dernière position.
	 * @param mot à ajouter, il sera stocké en minuscules (lowerCase)
	 */
	public void add(String mot) {
		mots.add(mot.toLowerCase());
	}

	/**
	 * Taille du dictionnaire, c'est à dire nombre de mots qu'il contient.
	 * @return la taille
	 */
	public int size() {
		return mots.size();
	}
	
	/**
	 * Accès au i-eme mot du dictionnaire.
	 * @param i l'index du mot recherché, compris entre 0 et size-1.
	 * @return le mot à cet index
	 */
	public String get(int i) {
		return mots.get(i);
	}

	/**
	 * Rend une copie de ce Dictionnaire.
	 * @return une copie identique de ce Dictionnaire
	 */
	public Dictionnaire copy () {
		Dictionnaire copy = new Dictionnaire();
		copy.mots.addAll(mots);
		if(cache != null){
			copy.cache = new EnsembleLettre[cache.length];
			for(int i=0; i<cache.length; i++){
				if(cache[i] != null)
					copy.cache[i] = cache[i].clone();
			}
		}
		return copy;
	}

	/**
	 * Retire les mots qui ne font pas exactement "len" caractères de long.
	 * Attention cette opération modifie le Dictionnaire, utiliser copy() avant de filtrer pour ne pas perdre d'information.
	 * @param len la longueur voulue 
	 * @return le nombre de mots supprimés
	 */
	public int filtreLongueur(int len) {
		List<String> cible = new ArrayList<>();
		int cpt=0;
		for (String mot : mots) {
			if (mot.length() == len)
				cible.add(mot);
			else
				cpt++;
		}
		mots = cible;
		if(cpt > 0)
			cache = null;
		return cpt;
	}
	
	/**
	 * retire les mots qui ne contiennent pas le caractere "c" à la i-ème postion
	 * @param le caractere à tester
	 * @param l'indice du caractere
	 * @return le nombres de mots filtrés
	 */
	public int filtreParLettre(char c, int i){
		List<String> cible = new ArrayList<String>();
		int cpt=0;
		for (String mot : mots) {
			if (mot.charAt(i) == c)
				cible.add(mot);
			else
				cpt++;
		}
		mots = cible;
		if(cpt > 0)
			cache = null;
		return cpt;
	}
	/**
	 * filtre le dictionnaire pour ne garder que les mots dont l'i-ème caractere 
	 * se trouve dans l'ensemble de caractere
	 * @param l'ensemble de caracteres
	 * @param l'indice de des caracteres cherchés
	 * @return le nombre de mots filtrés
	 */
	public int filtreParEnsemble(EnsembleLettre ens, int i){
		List<String> cible = new ArrayList<String>();
		int cpt=0;
		for (String mot : mots) {
			if (ens.contains(mot.charAt(i)))
				cible.add(mot);
			else
				cpt++;
		}
		mots = cible;
		if(cpt > 0)
			cache = null;
		return cpt;
	}
	/**
	 * filtre le dictionnaire par rapport l'indice i
	 * @param i
	 * @return l'ensemble des lettres qui sont à la position i
	 */
	public EnsembleLettre filtreParIndice(int i) {
		if (this == null)
			return null;
		if (cache == null) {
			cache = new EnsembleLettre[mots.get(0).length()];
		}
		if (cache[i] == null) {
			cache[i] = new EnsembleLettre();
			for (String mot : mots) {
				cache[i].add(mot.charAt(i));
			}
		}
		return cache[i];
	}
	
	@Override
	public String toString() {
		if (size() == 1) {
			return mots.get(0);
		} else {
			return "Dico size =" + size();
		}
	}
	/**
	 * charge un dictionnaire depuis le fichier text path
	 * en supposant que chaque ligne contient un et un seul mot
	 * @param le nombre le nom du fichier
	 * @return le dictionnaire crée
	 */
	public static Dictionnaire loadDictionnaire(String path){
		Dictionnaire dic = new Dictionnaire();
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			for(String line = br.readLine(); line != null; line = br.readLine()){
				dic.add(line);
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return dic;
	}
	/**
	 * rende les mot du dictionnaire
	 * @return la liste des mots du dictionnaire
	 */
	public List<String> getMots(){
		return mots;
	}
	
}
