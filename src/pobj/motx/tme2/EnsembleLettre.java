package pobj.motx.tme2;

import java.util.*;
/**
 * classe représentant un ensemble de caractere
 */

import javax.swing.text.AbstractDocument.Content;

import pobj.motx.tme1.Case;

@SuppressWarnings("unused")
public class EnsembleLettre {
	/**
	 * liste representant un ensemble de caractere
	 */
	private List<Character> ens;
	/**
	 * constructeur d'un ensemble de lettres
	 */
	public EnsembleLettre() {
		ens = new ArrayList<Character>();
	}
	/**
	 * accesseur de l'ensemble de caracters
	 * @return
	 */
	public List<Character> getEnsCaract(){
		return ens;
	}
	/**
	 * ajouter le caractere "c" dans l'ensemeble
	 * @param le caractere à ajouter
	 */
	public void add(Character c){
		if(!ens.contains(c))
			ens.add(c);
	}
	/**
	 * calcule l'intersection de l'ensemble courant avec l'ensemble en argument
	 * @param l'ensemble à avec lequel comparer
	 * @return le resultat de l'intersection
	 */
	public EnsembleLettre intersection(EnsembleLettre e){
		EnsembleLettre inter = new EnsembleLettre();
		for(Character c : ens){
			if(e.getEnsCaract().contains(c))
				inter.add(c);
		}
		return inter;
	}
	/**
	 * tester si le caractere est present dans l'ensemble
	 * @param le caractere à tester
	 * @return 
	 */
	public boolean contains(Character c){
		return ens.contains(c);
	}
	/**
	 * renvoie la taille de l'ensemble
	 * @return
	 */
	public int size(){
		return ens.size();
	}
	/**
	 * renvoie la copie de l'ensemble actuel
	 */
	public EnsembleLettre clone(){
		EnsembleLettre copy = new EnsembleLettre();
		copy.ens.addAll(ens);
		return copy;
	}
	
}
