package pobj.motx.tme2;

import java.util.ArrayList;
import java.util.List;

import pobj.motx.tme1.Case;
import pobj.motx.tme1.GrilleMots;
import pobj.motx.tme1.Mot;

/**
 * classe représentant une grille potentielle à partir d'une grille
 * @author 3504824
 *
 */

public class GrillePotentiel {
	private GrilleMots grille;
	private Dictionnaire dico;
	private List<Dictionnaire> motPot ;
	List<IContrainte> contraintes;
	
	/**
	 * construit une grille potentielle à partir de la grille "g" et du dictionnaire dico
	 * @param grille
	 * @param dico
	 */
	public GrillePotentiel(GrilleMots grille, Dictionnaire dico){
		this.grille = grille;
		this.dico = dico;
		motPot = new ArrayList<Dictionnaire>();
		contraintes = new ArrayList<>();
		/* creer un dictionnaire pour chaque mot en fonction de sa longueur
		 * et chaque caractere du mot à partir du dictionnaire initial
		 */
		List<Mot> mot = grille.getMots();
		for(Mot m : mot){
			Dictionnaire tmp = dico.copy();
			tmp.filtreLongueur(m.size());
			/*parcours les lettres de chaque mot*/
			for(int i=0; i<m.size(); i++){
				Case c = m.getLettres().get(i);
				if(!c.isVide()){
					tmp.filtreParLettre(c.getChar(), i);
				}			
			}
			motPot.add(tmp);		
		}

		/*parcours des mots horinzontaux*/
		for(int m1=0; m1<grille.getNbHorizontal(); m1++){
			Mot mot1 = grille.getMots().get(m1);
			/*parcours des mots verticaux*/
			for(int m2=grille.getNbHorizontal(); m2<grille.getMots().size(); m2++){
				Mot mot2 = grille.getMots().get(m2);
				/*parcours des lettres de chaque mot horizontal*/
				for(int c1=0; c1<mot1.size(); c1++){
					/*parcours de chaque de mot vertical*/
					for(int c2=0; c2<mot2.size(); c2++){
						int l1 = mot1.getLettres().get(c1).getLig();
						int l2 = mot2.getLettres().get(c2).getLig();
						int col1 = mot1.getLettres().get(c1).getCol();
						int col2 = mot2.getLettres().get(c2).getCol();
						if(l1 == l2 && col1 == col2){
							if(mot1.getLettres().get(c1).isVide()){
								contraintes.add(new CroixContrainte(m1, m2, c1, c2));
							}
								
						}
					}
				}
			}
		}
		propage();
	}
	/**
	 * construire une grille potentielle avec la liste des dictionnaires
	 * potentiels de chaque mot pour éviter la recherche dans le dictionnaire initial
	 * @param grille
	 * @param dico
	 * @param dicoPot
	 */
	public GrillePotentiel(GrilleMots grille, Dictionnaire dico, List<Dictionnaire> dicoPot){
		this.grille = grille;
		this.dico = dico;
		motPot = dicoPot;
		contraintes = new ArrayList<>();
		/* creer un dictionnaire pour chaque mot en fonction de sa longueur
		 * et chaque caractere du mot à partir du dictionnaire initial
		 */
		List<Mot> mot = grille.getMots();
		for(int j=0; j<mot.size(); j++){
			Mot m = mot.get(j);
			Dictionnaire tmp = motPot.get(j).copy();
			tmp.filtreLongueur(m.size());
			/*parcours les lettres de chaque mot*/
			for(int i=0; i<m.size(); i++){
				Case c = m.getLettres().get(i);
				if(!c.isVide()){
					tmp.filtreParLettre(c.getChar(), i);
				}			
			}
			motPot.add(tmp);		
		}

		/*parcours des mots horinzontaux*/
		for(int m1=0; m1<grille.getNbHorizontal(); m1++){
			Mot mot1 = grille.getMots().get(m1);
			/*parcours des mots verticaux*/
			for(int m2=grille.getNbHorizontal(); m2<grille.getMots().size(); m2++){
				Mot mot2 = grille.getMots().get(m2);
				/*parcours des lettres de chaque mot horizontal*/
				for(int c1=0; c1<mot1.size(); c1++){
					/*parcours de chaque de mot vertical*/
					for(int c2=0; c2<mot2.size(); c2++){
						int l1 = mot1.getLettres().get(c1).getLig();
						int l2 = mot2.getLettres().get(c2).getLig();
						int col1 = mot1.getLettres().get(c1).getCol();
						int col2 = mot2.getLettres().get(c2).getCol();
						if(l1 == l2 && col1 == col2){
							if(mot1.getLettres().get(c1).isVide()){
								contraintes.add(new CroixContrainte(m1, m2, c1, c2));
							}
								
						}
					}
				}
			}
		}
		propage();
	}
	/**
	 * accesseur de la listes de dictionnaire des mots potentiels
	 * @return
	 */
	public List<Dictionnaire> getMotsPot(){
		return motPot;
	}
	/**
	 * renvoie vrai s'il existe un mot de dictionnaire vide
	 * @return
	 */
	public boolean isDead(){
		for(Dictionnaire d : motPot)
		if(d.size()==0){
			return true;
		}
		return false;
	}
	/**
	 * construire une nouvelle grille dont le m-ème mot a pour valeur soluce
	 * @param l'indice du mot à remplir
	 * @param la valeur à donner
	 * @return la nouvelle grille
	 */
	public GrillePotentiel fixer(int m, String soluce){
		GrilleMots gr = grille.fixer(m, soluce);
		Dictionnaire d = dico.copy();
		return new GrillePotentiel(gr, d, motPot);
	}
	/**
	 * 
	 * @return
	 */
	private boolean propage(){
		while(true){
			int cpt=0;
			for(IContrainte ic : contraintes){
				cpt += ic.reduce(this);
			}
			if(isDead()){
				return false;
			}
			if(cpt==0)
				return true;
		}
		
	}
	public List<IContrainte> getContraintes() {
		return contraintes;
	}
	public GrilleMots getGrilleMot(){
		return grille;
	}
	
}
