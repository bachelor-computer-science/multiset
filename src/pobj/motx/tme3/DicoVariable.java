package pobj.motx.tme3;

import java.util.ArrayList;
import java.util.List;

import pobj.motx.tme2.GrillePotentiel;
/**
 * Classe représentant le domaine d'un mot 
 * @author 3504824
 *
 */
public class DicoVariable implements IVariable{
	/**
	 * l'indice du mot à traiter
	 */
	private int index; 
	/**
	 * Grille potentielle qui contient les mots
	 */
	private GrillePotentiel gr;
	/***
	 * Construit le domain d'un mot
	 * @param l'indice du dans la grille 
	 * @param grille potentielle contenant les mots potentiels
	 */
	public DicoVariable(int i, GrillePotentiel g){
		index = i;
		gr = g;
	}
	
	public int getIndex(){ return index;}
	
	public GrillePotentiel getGrille(){return gr;}
	
	public List<String> getDomain(){
		List<String> domain = new ArrayList<>();
		domain.addAll(gr.getMotsPot().get(index).getMots());
		return domain;
		
	}
	public String toString(){
		return gr.getGrilleMot().toString();
	}
	

}
