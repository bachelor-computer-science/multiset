package pobj.motx.tme3;

public interface IChoixVar {
	
	IVariable chooseVar(ICSP problem);

}
