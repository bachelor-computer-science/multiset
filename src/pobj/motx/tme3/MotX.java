package pobj.motx.tme3;


//import java.awt.List;
import java.util.ArrayList;
import java.util.List;

import pobj.motx.tme2.GrillePotentiel;

public class MotX implements ICSP{
	private GrillePotentiel grille;
	private List<IVariable> listD ;

	
	public MotX(GrillePotentiel gr){
		grille = gr;
		listD = new ArrayList<>();
		for(int i=0;i<gr.getGrilleMot().getMots().size();i++){
			for(int j=0;j<gr.getGrilleMot().getMots().get(i).size();j++){
				if(gr.getGrilleMot().getMots().get(i).getLettres().get(j).isVide()){
					listD.add(new DicoVariable(i, gr));
					break;
				}
			}
		}
	}	
	@Override
	public List<IVariable> getVars() {

		return listD;
	}
	@Override
	public boolean isConsistent() {
		return !grille.isDead();
	}
	@Override
	public ICSP assign(IVariable vi, String val) {
		if(vi instanceof DicoVariable){
			return new MotX(grille.fixer(((DicoVariable) vi).getIndex(), val));
		}
		return this;
	}
	
	public GrillePotentiel getGrillePot() {
		return grille;
	}

}
