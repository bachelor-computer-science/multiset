package pobj.motx.tme3;

public class StratMin implements IChoixVar{

	@Override
	public IVariable chooseVar(ICSP problem) {
		IVariable IvarMin = problem.getVars().get(0);
		for(int i=1; i<problem.getVars().size(); i++){
			if(problem.getVars().get(i).getDomain().size() < IvarMin.getDomain().size())
				IvarMin = problem.getVars().get(i);
		}
		return IvarMin;
	}

}
