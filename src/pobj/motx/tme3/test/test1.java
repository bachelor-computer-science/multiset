package pobj.motx.tme3.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import pobj.motx.tme1.Grille;
import pobj.motx.tme1.GrilleLoader;
import pobj.motx.tme1.GrilleMots;
import pobj.motx.tme2.Dictionnaire;
import pobj.motx.tme2.GrillePotentiel;
import pobj.motx.tme3.CSPSolver;
import pobj.motx.tme3.ICSP;
import pobj.motx.tme3.MotX;

public class test1 {


@Test
public void testLarge3() {

	Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
	Grille gr = GrilleLoader.loadGrille("data/large.grl");
	
	System.out.println(gr.toString());

	GrilleMots grille = new GrilleMots(gr);

	GrillePotentiel gp = new GrillePotentiel(grille, gut);
	ICSP problem=new MotX(gp);
	CSPSolver csp = new CSPSolver();
	ICSP res = csp.solve(problem);
	MotX resultat = (MotX)(res);
	System.out.println(resultat.getGrillePot().getGrilleMot().getGrille().toString());
	
	System.out.println("Succès test GrillePotentiel : large3.");
}
}